FROM ruby:2.3.1

MAINTAINER Michal Szymanski <michal.t.szymanskih@gmail.com>
RUN apt-get update -qq && apt-get install -y libzmq3-dev xvfb libossp-uuid-dev nodejs mysql-client postgresql-client sqlite3 --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/

RUN bundle install
COPY . /usr/src/app

RUN rails new --skip-gemfile /usr/src/app

EXPOSE 3000
